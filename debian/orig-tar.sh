#!/bin/sh -e

# At the moment this script doesn't work correctly, because the
# upstream codebase must be manually merged with the emfplus branch
# available here:
#
# http://java.freehep.org/svn/repos/freehep/list/freehep/branches/vectorgraphics-emfplus/freehep-graphicsio-emf/?revision=HEAD&bypassEmpty=true

VERSION=$2
DOWNLOADED_FILE=$3
PACKAGE=$(dpkg-parsechangelog | sed -n 's/^Source: //p')
TAR=../${PACKAGE}_${VERSION}+dfsg1.orig.tar.gz
DIR=${PACKAGE}-${VERSION}

svn export svn://svn.freehep.org/svn/freehep/tags/vectorgraphics-${VERSION}/$PACKAGE $DIR
rm -f $DIR/src/test/resources/emf/*.emf
rm -f $DIR/src/test/resources/emf/*.emf.gz
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR

rm -f $DOWNLOADED_FILE

