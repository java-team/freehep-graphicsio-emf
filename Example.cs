using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace BasicDrawingSample
{
	class EllipseDrawer
	{
		public Image Draw(int width, int height, int 
strokeWidth, Color strokeColor, Color fillColor)
		{
			// create the bitmap we will draw to
             Image image = new Bitmap(width + strokeWidth, 
height + strokeWidth);

			// calculate the half of the stroke 
width for use later
			float halfStrokeWidth = strokeWidth / 2F;

			// create a rectangle that bounds the 
ellipse we want to draw
			RectangleF ellipseBound = new RectangleF(
				halfStrokeWidth, halfStrokeWidth,
				width, height);

			// create a Graphics object from the bitmap
			using(Graphics graphics = 
Graphics.FromImage(image))
			{
				// create a solid color brush
				using(Brush fillBrush = new 
SolidBrush(fillColor))
				{
					// fill the ellipse 
specified by the rectangle calculated above
                     graphics.FillEllipse(fillBrush, ellipseBound);
				}

				// create a pen
				using(Pen pen = new 
Pen(strokeColor, strokeWidth))
				{
					// draw the stroke of 
the ellipse specified by the rectangle calculated above
					
graphics.DrawEllipse(pen, ellipseBound);
				}
			}

			return image;
		}

		[STAThread]
		static void Main(string[] args)
		{	
			if(args.Length != 5)
			{
				Console.WriteLine("Usage: 
ellipseDrawer width height stroke-width stroke-color fill-color");
			}
			else
			{
				// get values from the command 
line arguments
				int width = Int32.Parse(args[0]);
				int height = Int32.Parse(args[1]);
				int strokeWidth = Int32.Parse(args[2]);
				Color strokeColor = 
ColorTranslator.FromHtml(args[3]);
				Color fillColor = 
ColorTranslator.FromHtml(args[4]);

				// create and instance of the 
EllipseDrawer
				EllipseDrawer ellipseDrawer = 
new EllipseDrawer();

				// draw our ellipse
				Image image = 
ellipseDrawer.Draw(width, height, strokeWidth, strokeColor, 
fillColor);

				// save to a file
				image.Save("ellipse.png", 
ImageFormat.Png);

				// clean up the image resources
				image.Dispose();
			}
		}
	}
}

