// Copyright 2005-2006, FreeHEP.
package org.freehep.graphicsio.emf.test;

import org.freehep.graphicsio.test.TestSuite;

/**
 * @author Mark Donszelmann
 * @version $Id$
 */
public class EMFTestSuite extends TestSuite {

    public static TestSuite suite() {
        EMFTestSuite suite = new EMFTestSuite();
// FIXME: re-enable
//        suite.addTests("EMF");
        suite.addTests("EMF+");
        return suite;
    }

}
